import React from 'react';
import './App.css';
import PedidosPage from './Modules/Pedidos/PedidosPage';

function App() {
  return (
    <div className="App">
      <h1>FULL DEMOS</h1>
      <PedidosPage />
    </div>
  );
}

export default App;
