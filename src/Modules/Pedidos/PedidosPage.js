import React, { useEffect, useState, Fragment } from 'react';
import ListDemo from '../../Components/ListDemo';



const PedidosPage = () => {
    const [ListaPedidos, setListaPedidos] = useState([]);
    useEffect(() => {
        // fetch('https://rickandmortyapi.com/api/character/?name=slow&status=alive')
        fetch('https://rickandmortyapi.com/api/character/')
            .then(function (response) {
                return response.json();
            })
            .then(function (myJson) {
                //console.log(myJson.results);
                setListaPedidos(myJson.results);
            });
    }, []);

    return (
        <Fragment>
            <ListDemo listPedidos={ListaPedidos} />
        </Fragment>
    );
}

export default PedidosPage;
