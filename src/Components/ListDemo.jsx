import React, { useEffect, useState, Fragment } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Checkbox from '@material-ui/core/Checkbox';
import IconButton from '@material-ui/core/IconButton';
import { AccessAlarm, FilterBAndWRounded } from '@material-ui/icons';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Grid from '@material-ui/core/Grid';
import CardHeader from '@material-ui/core/CardHeader';
import Avatar from '@material-ui/core/Avatar';
const useStyles = makeStyles({
    root: {
        //  minWidth: 275,
        // background: 'yellow',
        border: 8,
        borderColor: 'black',

    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
    },
    CardContents: {
        // marginBottom: 12,
        background: 'gray',
        // border: 4,
        // borderColor: 'black',
        // flex: 1
    },
    grids: {
        flexGrow: 1,
    },
    avatar: {
        backgroundColor: 'blue',
    },
});
const ListDemo = ({ listPedidos }) => {
    const classes = useStyles();
    return (
        <Fragment>
            <Grid container item xs={12} spacing={1}>
                <Grid container className={classes.grids} item xs={12} spacing={3}>
                    {listPedidos.map(todo => (
                        <Card key={todo.id.toString()} className={classes.root}>
                            <CardHeader avatar={
                                <Avatar aria-label="recipe" className={classes.avatar}>R</Avatar>
                            }
                                action={
                                    <IconButton aria-label="settings">
                                        <FilterBAndWRounded />
                                    </IconButton>
                                }
                                title="Shrimp and Chorizo Paella"
                                subheader="September 14, 2016"
                            />
                            <CardContent className={classes.CardContents}>
                                {todo.name}
                                <List>
                                    {todo.episode.map((epi, index) => (
                                        <ListItem key={index.toString()}>
                                            <Checkbox tabIndex={-1} disableRipple />
                                            <ListItemText primary={epi} />
                                        </ListItem>
                                    ))}
                                </List>

                            </CardContent>
                        </Card>
                        // <ListItem key={todo.id.toString()}>
                        //     <ListItemText primary={':::mesa o pedido:::' + todo.name} />
                        //     <List>
                        //         {todo.episode.map((epi, index) => (
                        //             <ListItem key={index.toString()}>
                        //                 <Checkbox tabIndex={-1} disableRipple />
                        //                 <ListItemText primary={epi} />
                        //             </ListItem>
                        //         ))}
                        //     </List>
                        // </ListItem>
                    ))}
                </Grid></Grid>
        </Fragment>
    );
};

export default ListDemo;
